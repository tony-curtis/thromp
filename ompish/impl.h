#ifndef _IMPL_H
#define _IMPL_H 1

/*
 * overkill here, but used to encapsulate multiple args to a thread
 */

typedef struct args {
    int id;                     /* this thread's number */
} ompish_thread_args_t;

#endif  /* _IMPL_H */
