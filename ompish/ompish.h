#ifndef _OMPISH_H
#define _OMPISH_H 1

/*
 * handle to get at thread state (would be hidden by runtime API in
 * reality)
 */
typedef void *ompish_thread_args_h;

void ompish_threads_barrier_all(void);

void ompish_threads_init(void);

void ompish_threads_finalize(void);

void ompish_threads_launch(void *(fn)(void *));

int ompish_get_thread_num(ompish_thread_args_h t);

#endif  /* _OMPISH_H */
