#include "impl.h"
#include "ompish.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * often this is the number of processors in the machine
 * (e.g. see /proc/cpuinfo)
 */
#define DEFAULT_NUM_THREADS 1

/*
 * thread state management
 */
static pthread_t            *thread_pool;
static ompish_thread_args_t *thread_args;
static int                   max_num_threads;
static pthread_barrier_t     gb; /* global barrier */

void
ompish_threads_barrier_all(void)
{
    pthread_barrier_wait(&gb);
}

void
ompish_threads_init(void)
{
    const char *user_threads = getenv("OMP_NUM_THREADS");

    if (user_threads != NULL) {
        max_num_threads = strtol(user_threads, NULL, 10);
    }
    else {
        max_num_threads = sysconf(_SC_NPROCESSORS_ONLN);
    }
    if (max_num_threads < 0) {
        max_num_threads = DEFAULT_NUM_THREADS;
    }

    pthread_barrier_init(&gb, NULL, max_num_threads);

    /* NB not checking for successful allocation */
    thread_args = (ompish_thread_args_t *)
        malloc(max_num_threads * sizeof(*thread_args));
    thread_pool = (pthread_t *)
        malloc(max_num_threads * sizeof(*thread_pool));
}

void
ompish_threads_finalize(void)
{
    int i;

    /* wait for all threads to finish */
    for (i = 0; i < max_num_threads; ++i) {
        pthread_join(thread_pool[i], NULL);
    }

    pthread_barrier_destroy(&gb);

    free(thread_pool);
    free(thread_args);
}

void
ompish_threads_launch(void *(*fn)(void *))
{
    int i;

    /* create and run threads: pass id of thread to each */
    for (i = 0; i < max_num_threads; ++i) {
        thread_args[i].id = i;
        pthread_create(&thread_pool[i],
                       NULL,
                       fn,
                       (void *) &thread_args[i]
                       );
    }
}

int
ompish_get_thread_num(ompish_thread_args_h t)
{
    /* unpack */
    const ompish_thread_args_t *thr = (ompish_thread_args_t *) t;

    return (thr != NULL) ? thr->id : 0;
}
