/*
 * Sub-OpenMP-like hello world program
 *
 * cf. basic OpenMP version
 *
 *      #include <stdio.h>
 *      #include <omp.h>
 *
 *      int
 *      main(void)
 *      {
 *      #pragma omp parallel
 *          printf("Hello from thread %d\n", omp_get_thread_num());
 *
 *          return 0;
 *      }
 *
 */

#include <stdio.h>

#include "ompish.h"

/*
 * function that is run inside each thread
 */
void *
outlined_hello_world(void *thread)
{
    /* ONE LINE OF ACTUAL WORK */
    printf("Hello from thread %d\n", ompish_get_thread_num(thread));

    return NULL;
}

int
main(void)
{
    ompish_threads_init();

    ompish_threads_launch(outlined_hello_world);

    ompish_threads_finalize();

    return 0;
}
