include make.common

CFLAGS += -I./ompish
LDFLAGS += -L./ompish

all:	a.out

a.out:	ompish-all hello.o
	$(LD) $(LDFLAGS) -o $@ hello.o $(LDLIBS)

ompish-all:
	$(MAKE) -C ompish all

tidy:
	$(MAKE) -C ompish $@
	rm -f *.o

clean:	tidy
	$(MAKE) -C ompish $@
	rm -f a.out
